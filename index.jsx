/* jshint globalstrict: true, esnext: true, browser: true */

'use strict';

import React from 'react';
import GifList from './gif-list';

React.render(
  <GifList />,
  document.getElementById('body')
);

