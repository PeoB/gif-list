# Gif List

Showcasing how to use React, ECMAScript 6 and RxJS.

## Usage

```zsh
npm install
npm run build
```

Optionally, to run from a web server:

```zsh
npm install -g live-server
live-server
```
