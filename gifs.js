/* jshint globalstrict: true, esnext: true, browser: true */

'use strict';

import { Observable} from 'rx';

function get(url) {
  return new Promise((res,rej)=>{
	var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = ()=> req.status===200?
		res(req.response):
		rej(Error(req.statusText));

    // Handle network errors
    req.onerror = ()=>rej(Error("Network Error"));

    // Make the request
    req.send();
  });
}

class Gifs {
  url(after) {
    return 'http://www.reddit.com/r/gif.json' +
      (after ? '?after=' + after : '');
  }

  entries(after) {
	var jsonRes=get(this.url(after))
		.then(JSON.parse);
		
    return Observable.fromPromise(jsonRes)
		.pluck('data')
		.flatMap(d=>{
			var entries=Observable.from(d.children).pluck('data');
			return d.after?
				entries.concat(this.entries(d.after)): //one more page
				entries; //last page
		})
		.filter(entry => /.gif$/.test(entry.url));
  }
}

export default new Gifs();

